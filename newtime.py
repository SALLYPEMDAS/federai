from datetime import datetime

def newtime(oldtime = datetime.utcnow().__format__('%Y%m%d%H%M%S')):
    print oldtime
    yyyymmdd = oldtime[:8]
    hhmmss = oldtime[8:]
    hr = hhmmss[:2]
    min = hhmmss[2:4]
    sec = hhmmss[4:]
    hr = int(hr)
    min  = int(min)
    sec = int(sec)
    seconds = (hr * (60 * 60)) + (min * 60) + sec
    fortminute = (seconds/864) - 1
    secs = seconds - (864 * fortminute)
    print(str(fortminute) + ":" + str(seconds))

def fortminute(oldtime = datetime.utcnow().__format__('%Y%m%d%H%M%S')):
    yyyymmdd = oldtime[:8]
    hhmmss = oldtime[8:]
    hr = hhmmss[:2]
    min = hhmmss[2:4]
    sec = hhmmss[4:]
    hr = int(hr)
    min  = int(min)
    sec = int(sec)
    seconds = (hr * (60 * 60)) + (min * 60) + sec

    return int((seconds/864)) - 1

def _seed(time = datetime.utcnow().__format__('%Y%m%d%H%M%S')):
    return time[:8] + str(fortminute(time))